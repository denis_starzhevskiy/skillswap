import React from 'react';
import {Box, Button, Container, Grid, Typography} from "@mui/material";
import headerView from "../assets/header_view.jpg"
import AcUnitIcon from '@mui/icons-material/AcUnit';
import TravelExploreIcon from '@mui/icons-material/TravelExplore';
import AccessibilityNewIcon from '@mui/icons-material/AccessibilityNew';
import BoltIcon from '@mui/icons-material/Bolt';

const blocks = [
    {
        title: "Explore Endless Opportunities",
        icon: (props) => <TravelExploreIcon {...props}/>,
        description: 'Looking for your dream job? Our job hire platform connects talented individuals like you with a wide range of exciting career opportunities. Unleash your potential and take the first step towards a fulfilling professional journey.'
    }, {
        title: "Simplified Hiring Process",
        icon: (props) => <AccessibilityNewIcon {...props}/>,
        description: 'Employers, streamline your recruitment process with our user-friendly platform. Post job listings, manage applicants effortlessly, and find the perfect match for your company culture. Say goodbye to hiring headaches and embrace the efficiency of our tailored hiring solutions.'
    }, {
        title: "Empowering Your Future",
        icon: (props) => <BoltIcon {...props}/>,
        description: 'We believe in empowering individuals to achieve their career aspirations. With our job hire website, you\'re not just applying for jobs – you\'re embracing a world of possibilities, personal growth, and unparalleled success. Join our community and take charge of your future today!'
    },
]

const Home = () => {
    return (
        <>
        <Container maxWidth={'lg'} sx={{minHeight: "100vh"}}>
            <Box sx={{marginTop: '120px', minHeight: '500px', display: 'grid', gridTemplateColumns: {xs: '1fr', md: "1.5fr 1fr"}, alignItems: 'center'}}>
                <Box>
                    <Box sx={{width: "50%"}}>
                        <Button color={"primary"}>New</Button>
                    </Box>
                    <Typography style={{fontSize: "50px", fontWeight: 700, color: "#030407", letterSpacing: '-1px', maxWidth: "400px"}}>
                        Find the most exciting jobs in tech
                    </Typography>
                </Box>
                <Typography style={{fontSize: "20px", fontWeight: 400, color: "#030407", letterSpacing: '-1px'}}>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed a mi rhoncus, pharetra leo et. Sign in or sign up to get started!
                </Typography>
            </Box>
            <Box sx={{padding: {xs: "10px 5px", md: '10px 50px'}}}>
                <img src={headerView} alt={"logo"} style={{maxWidth: "100%"}}/>
            </Box>
            <Grid container sx={{padding: '100px 0px', rowGap: '20px'}}>
                <Grid item xs={12} sx={{display: 'flex', alignItems: 'center', columnGap: '10px'}}>
                    <AcUnitIcon sx={{color: theme => theme.palette.background.main}} fontSize={"large"}/>
                    <Typography variant={'h6'} sx={{fontWeight: 600}}>How it works</Typography>
                </Grid>
                <Grid item xs={12} md={6} sx={{marginBottom: {xs: '40px',md: '0px'}}}>
                    <Typography variant={'h5'} sx={{fontWeight: 600, maxWidth: "90%"}}>
                        Succeed Together, Find Your Future: Your Path to Professional Excellence!
                    </Typography>
                </Grid>
                <Grid item xs={12} md={6}>
                    {blocks.map((item, key) => {
                        const Icon = item.icon

                        return <Box key={key} sx={{marginBottom: "50px"}}>
                            <Box sx={{display: 'flex', columnGap: '10px', alignItems: 'center', marginBottom: "5px"}}>
                                <Icon/>
                                <Typography variant={"h6"}>{item.title}</Typography>
                            </Box>
                            <Typography>{item.description}</Typography>
                        </Box>
                    })}
                </Grid>
            </Grid>
        </Container>
        <Box sx={{minHeight: '500px', backgroundColor: theme => theme.palette.background.default, padding: {xs: "0px 20px", md: "0px 100px"}}}>
            <Grid container sx={{padding: '100px 0px', rowGap: '20px'}}>
                <Grid item xs={12} sx={{display: 'flex', alignItems: 'center', columnGap: '10px'}}>
                    <AcUnitIcon sx={{color: 'white'}} fontSize={"large"}/>
                    <Typography variant={'h6'} sx={{fontWeight: 600, color: "white"}}>How it works</Typography>
                </Grid>
                <Grid item xs={12} md={6} sx={{marginBottom: {xs: '40px',md: '0px'}}}>
                    <Typography variant={'h5'} sx={{fontWeight: 600, maxWidth: "90%", color: "white"}}>
                        Succeed Together, Find Your Future: Your Path to Professional Excellence!
                    </Typography>
                </Grid>
                <Grid item xs={12} md={6}>
                    {blocks.map((item, key) => {
                        const Icon = item.icon
                        return <Box key={key} sx={{marginBottom: "50px"}}>
                            <Box sx={{display: 'flex', columnGap: '10px', alignItems: 'center', marginBottom: "5px"}}>
                                <Icon sx={{color: 'white'}}/>
                                <Typography variant={"h6"} sx={{color: "white"}}>{item.title}</Typography>
                            </Box>
                            <Typography sx={{color: "white"}}>{item.description}</Typography>
                        </Box>
                    })}
                </Grid>
            </Grid>
        </Box>
        </>
    );
};

export default Home;