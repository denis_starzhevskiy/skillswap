import './App.css';
import Home from "./pages/Home";
import {AppBar, Box, Button, Typography} from "@mui/material";
import logo from './assets/logoSkillShare.png'
import {ArrowRightAltOutlined} from "@mui/icons-material";

const pages = ['Products', 'Pricing', 'Blog'];

function App() {
  return <>
    <AppBar position={"absolute"} sx={{height: "100px", background: 'transparent', display: "flex", alignItems: "center", flexDirection: 'row', px: {xs: '5px', md: '40px'}, columnGap: '50px'}}>
      {/*<IconButton>*/}
      {/*  <SwapCallsIcon fontSize={"large"}/>*/}
      {/*  <Typography sx={{fontSize: '30px'}}>SkillSwap</Typography>*/}
      {/*</IconButton>*/}
      <Box sx={{display: "flex", alignItems: 'center'}}>
        <img src={logo} height={'100px'} alt={"logo"}/>
        <Typography sx={{color: "#3e404c", fontSize: {xs: '20px', md: "30px"}, letterSpacing: "0.1rem"}}>SkillSwap</Typography>
      </Box>
      <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }}>
        {pages.map((page) => (
            <Button
                key={page}
                // onClick={handleCloseNavMenu}
                sx={{ color: '#3e404c', display: 'block', fontSize: '14px', fontWeight: 500 }}
            >
              {page}
            </Button>
        ))}
      </Box>
      <Box sx={{display: 'flex', columnGap: "20px"}}>
      {/*<Button*/}
      {/*    // onClick={handleCloseNavMenu}*/}
      {/*    sx={{ color: '#3e404c', display: 'block', fontSize: '14px', fontWeight: 500 }}*/}
      {/*>*/}
      {/*  Post a job*/}
      {/*</Button>*/}
      <Button
          color={"primary"}
          variant={'contained'}
          // onClick={handleCloseNavMenu}
          endIcon={<ArrowRightAltOutlined/>}
          sx={{ display: 'flex', fontSize: '14px', fontWeight: 500, color: 'white' }}
      >
        Register
      </Button>
      </Box>
    </AppBar>
    <Home/>
  </>
}

export default App;
